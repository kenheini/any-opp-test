'use strict';

/**
 * @ngdoc function
 * @name myTestApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the myTestApp
 */
angular.module('myTestApp').controller('LoginCtrl', LoginCtrl);

function LoginCtrl(authSvc, $location, $route) {
  var self = this;
  self.login = function () {
    self.onRequest = true;
    authSvc.login({
      username: this.username,
      password: this.password
    }, function (err) {
      self.onRequest = false;
      if (!err) {
        $location.path('detail');
        $route.reload();
      }
      else{
        if(typeof err === 'object'){
          self.errMessage = err.message;
        }
        else{
          self.errMessage = err;
        }
      }
    });
  };
};
