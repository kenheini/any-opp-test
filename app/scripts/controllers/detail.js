'use strict';

/**
 * @ngdoc function
 * @name myTestApp.controller:DetailCtrl
 * @description
 * # DetailCtrl
 * Controller of the myTestApp
 */
angular.module('myTestApp').controller('DetailCtrl', DetailCtrl);

function DetailCtrl(blobSvc, mapSvc) {
  var self = this;
  self.map = mapSvc.config;
  self.fileUploadeds = [];

  self.places = [];

  self.selectPlace = function () {
    if (typeof self.address === 'object') {
      if (!self.places[0]) {
        self.places[0] = {
          id: self.address.id
        };
      }
      self.map.center.latitude = self.places[0].latitude = self.address.geometry.location.lat();
      self.map.center.longitude = self.places[0].longitude = self.address.geometry.location.lng();
    }
  };

  self.uploadFile = function (file) {
    if (file) {
      self.onUpload = true;
      blobSvc.uploadFile(file, function (err, data) {
        self.onUpload = false;
        if (err) {
          console.log(err);
          alert('Upload failed.');
          return;
        } else {
          if (data.data[0]) {
            self.fileUploadeds.push(data.data[0]);
          } else {
            self.fileUploadeds.push(data.data);
          }
        }
      });
    }
  };
}
;
