'use strict';

angular.module('myTestApp')
  .factory('blobSvc', function (Upload, CONTS, authInterceptor) {
    var name = 'blob';
    var url = CONTS.apiUrl + '/' + name;
    return {
      uploadFile: function (file, callback) {
        Upload.upload({
          url: url + '?containerName=private',
          data: {file: file}
        }).then(function (resp) {
          callback(null, resp);
        }, function (resp) {
          callback(resp);
        });
      }
    };
  });
