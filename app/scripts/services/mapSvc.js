'use strict';

angular.module('myTestApp')
  .factory('mapSvc', function () {
    return {
      config: {
        center: {
          latitude: 45,
          longitude: 45
        },
        zoom: 4
      }
    };
  });
