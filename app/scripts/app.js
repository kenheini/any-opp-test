'use strict';

/**
 * @ngdoc overview
 * @name myTestApp
 * @description
 * # myTestApp
 *
 * Main module of the application.
 */
angular
  .module('myTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'google.places',
    'uiGmapgoogle-maps',
    'ngFileUpload'
  ])
  .config(function ($routeProvider, $httpProvider) {

    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/detail', {
        templateUrl: 'views/detail.html',
        controller: 'DetailCtrl',
        controllerAs: 'detail'
      })
      .otherwise({
        redirectTo: '/login'
      });
      
      $httpProvider.interceptors.push('authInterceptor');
  })
  .constant('CONTS', {
    apiUrl: 'https://system-dev.anyopp.com/api'
  })
  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers['X-ZUMO-AUTH'] = $cookieStore.get('token');
        }
        config.headers['ZUMO-API-VERSION'] = '2.0.0';
        return config;
      },
      // Intercept 401s and redirect you to login
      responseError: function (response) {
        if (response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  });
